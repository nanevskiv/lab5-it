﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace labfinal.Models
{
    public class Friend
    {
        [Range(0, 200)]
        public int Id { get; set; }
        [Display(Name="Friend Name")]
        public string Name { get; set; }
        public string Place { get; set; }
    }
}