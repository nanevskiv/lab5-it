﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(labfinal.Startup))]
namespace labfinal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
